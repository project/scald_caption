<?php

/**
 * Implements hook_form().
 */
function scald_caption_config_form($form, &$form_state) {

  $scald_types = array_keys(scald_types());

  foreach ($scald_types as $type) {
    $form['scald_caption_custom_caption_'.$type] = array(
      '#type' => 'textfield',
      '#title' => t(ucfirst($type).' custom caption'),
      '#default_value' => variable_get('scald_caption_custom_caption_'.$type, NULL),
      '#size' => 60,
      '#description' => t('If empty the default caption will be applied.'),
      '#required' => FALSE,
    );

    $form['scald_caption_tokens_'.$type] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('atom'),
      '#global_types' => TRUE,
      '#click_insert' => TRUE,
      '#dialog' => TRUE,
    );
  }

  return system_settings_form($form);
}

