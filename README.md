This module provides in the administrative area of Drupal an option to customize the Scald caption, without the need to implement the hook_scald_dnd_library_item_alter.

If the token required in caption customization was not available, it should be created according to the Drupal documentation.

Https://api.drupal.org/api/drupal/includes%21token.inc/7.x

